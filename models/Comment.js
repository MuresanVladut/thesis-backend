const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
    body: String,
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    article: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Article'
    }
}, {timestamps: true});

// Requires population of author
CommentSchema.methods.toJSONFor = function (user) {
    return {
        id: this._id,
        body: this.body,
        createdAt: this.createdAt,
        author: this.author.toProfileJSONFor(user)
    };
};

// Requires population of author
CommentSchema.methods.toJSONForWithArticle = function () {
    return {
        id: this._id,
        body: this.body,
        createdAt: this.createdAt,
        article: this.article.toJSONForWithComments(),
        author: this.author.toCommentJSON()
    };
};

mongoose.model('Comment', CommentSchema);
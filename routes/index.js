const router = require('express').Router();
const cool = require('cool-ascii-faces');

router.use('/api', require('./api'));
router.get('/cool', function(request, response) {
    response.send(cool());
});


module.exports = router;

const router = require('express').Router();
const passport = require('passport');
const mongoose = require('mongoose');
const Article = mongoose.model('Article');
const Comment = mongoose.model('Comment');
const User = mongoose.model('User');
const auth = require('../auth');

router.get('/', auth.optional, function(req, res, next){
    return Promise.all([
        Comment.find({})
            .limit(Number(5))
            .skip(Number(0))
            .sort({createdAt: 'desc'})
            .populate('article')
            .populate('author')
            .exec(),
        req.payload ? User.findById(req.payload.id) : null
    ]).then(function(results){
        const comments = results[0];

        return res.json({
            comments: comments.map(function(comment){
                return comment.toJSONForWithArticle();
            }),
        });
    }).catch(next);
});

module.exports = router;
